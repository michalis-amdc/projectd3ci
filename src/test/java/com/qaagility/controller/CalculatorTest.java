package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {
    	Calculator calculatorObj = new Calculator();
        int k = calculatorObj.add();
        assertEquals("Michalis is testing", 9, k);
    }
}